package ooss;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> klasses;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
        klasses = new ArrayList<Klass>();
    }

    @Override
    public String introduce() {

            StringBuilder result = new StringBuilder(super.introduce() + " I am a teacher. I teach Class ");
            if (klasses.size() == 0) {
                return super.introduce() + " I am a teacher.";
            }
            return result +
                    klasses.stream()
                    .map(Klass::getNumber)
                    .sorted(Comparator.comparing(Integer::valueOf))
                    .map(Object::toString)
                    .collect(Collectors.joining(", ")) + ".";
    }

    public void assignTo(Klass klass) {
        this.klasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.klasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return belongsTo(student.getKlass());
    }

    public void personSay(Student student, int number) {
        String result = String.format("I am %s, teacher of Class %d. I know %s become Leader."
                , this.getName(), number, student.getName());
        System.out.println(result);
    }
}
