package ooss;

import java.util.List;
import java.util.Objects;

public class Klass {
    private int number;
    private Student leader;
    private Person attachPerson;

    public Klass(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public int getNumber() {
        return number;
    }

    public void assignLeader(Student student) {
        if (student.getKlass() != this) {
            System.out.println("It is not one of us.");
            return;
        }
        this.leader = student;

        if (this.attachPerson != null) {
            attachPerson.personSay(student, this.number);
        }
    }

    public boolean isLeader(Student student) {
        return Objects.equals(this.leader, student);
    }

    public void attach(Person person) {
        this.attachPerson = person;
    }
}
