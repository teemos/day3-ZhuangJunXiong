package ooss;

public class Student extends Person {
    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);

    }

    @Override
    public String introduce() {
        if (klass != null) {
            String result = super.introduce() + " I am a student.";
            if (klass.isLeader(this)) {
                result += String.format(" I am the leader of class %d.", klass.getNumber());
            } else {
                result += String.format(" I am in class %d.", klass.getNumber());
            }
            return result;
        }
        return super.introduce() + " I am a student.";
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return this.klass == klass;
    }

    public Klass getKlass() {
        return klass;
    }

    public void personSay(Student student, int number) {
        String result = String.format("I am %s, student of Class %d. I know %s become Leader."
                , this.getName(), number, student.getName());
        System.out.println(result);
    }
}
