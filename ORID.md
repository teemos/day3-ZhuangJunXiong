# ORID

## O

- Code review: This morning, we reviewed the code for last night's homework, learned from each other's classmates' code, and provided suggestions for modifying the code. I think this is a great way to improve code proficiency. By observing others' code, we can learn different problem-solving ideas and some coding techniques.
- Stream API: In the morning, we learned the concepts and usage of the Stream API and conducted a series of exercises. I have never used the Stream API before, which was a bit tricky for me at first. However, after my teacher's explanation and consulting online materials, I found that the Stream API is actually quite simple and can optimize the code we write, making it simpler and more readable.
- OO: We spent the entire afternoon learning object-oriented programming, and OO is also something we are familiar with. Its characteristics are encapsulation, inheritance, and polymorphism. OO is an indispensable part of programming that we cannot bypass. Its advantage is that it makes the code easy to maintain, reuse, and expand.

## R

Good

## I

I think code review is a great way to improve code proficiency. By observing others' code, we can learn different problem-solving ideas and some coding techniques.Meanwhile, the Stream API is also a great tool for us to write code with.

## D

When writing code in the future, I will try using the Stream API to write code instead of just like before